CREATE TABLE dice
(
    ID             INT PRIMARY KEY AUTO_INCREMENT NOT NULL,
    no_dice        INT                            NOT NULL,
    total_side     INT                            NOT NULL,
    total_no_rolls INT                            NOT NULL
);


CREATE TABLE roll
(
    ID      INT PRIMARY KEY AUTO_INCREMENT NOT NULL,
    sum     INT                            NOT NULL,
    dice_id INT                            NOT NULL,
    foreign key (dice_id) references dice (id)
);

