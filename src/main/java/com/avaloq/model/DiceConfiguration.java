package com.avaloq.model;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;


@Getter
@Setter
@ToString
public class DiceConfiguration {

    private Long id;
    private int diceNumber;
    private int totalSide;
    private int totalNoRolls;

    public DiceConfiguration(int diceNumber, int totalSide, int totalNoRolls) {
        this.diceNumber = diceNumber;
        this.totalSide = totalSide;
        this.totalNoRolls = totalNoRolls;
    }

}
