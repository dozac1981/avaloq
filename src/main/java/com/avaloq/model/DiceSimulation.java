package com.avaloq.model;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class DiceSimulation {
    private int noDice;
    private int totalSide;
    private int totalRolls;
    private int totalSimulation;

    public DiceSimulation(int noDice, int totalSide, int totalRolls, int totalSimulation) {
        this.noDice = noDice;
        this.totalSide = totalSide;
        this.totalRolls = totalRolls;
        this.totalSimulation = totalSimulation;
    }

    public DiceSimulation() {
    }
}
