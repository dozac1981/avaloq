package com.avaloq.model;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class DiceRelativeDistribution {
    private int sum;
    private int sumTotal;
    private int rollCount;
    private double relativeDistribution;


    public DiceRelativeDistribution(int sum, int sumTotal, int rollCount) {
        this.sum = sum;
        this.sumTotal = sumTotal;
        this.rollCount = rollCount;
    }

    public DiceRelativeDistribution(int sum, int sumTotal, int rollCount, double relativeDistribution) {
        this.sum = sum;
        this.sumTotal = sumTotal;
        this.rollCount = rollCount;
        this.relativeDistribution = relativeDistribution;
    }

    public DiceRelativeDistribution() {
    }

}
