package com.avaloq.model;

import lombok.Getter;
import lombok.Setter;


@Getter
@Setter
public class Roll {

    private Long id;
    private int sum;
    private Long diceId;

    public Roll(int sum, Long diceId) {
        this.sum = sum;
        this.diceId = diceId;
    }

    public Roll(int sum) {
        this.sum = sum;
    }
}
