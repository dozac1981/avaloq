package com.avaloq.repository;

import com.avaloq.model.DiceConfiguration;
import com.avaloq.model.DiceSimulation;

import java.util.List;

public interface DiceDao {

    long saveDice(DiceConfiguration diceConfiguration);

    List<DiceSimulation> getDiceSimulation();
}
