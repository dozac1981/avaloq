package com.avaloq.repository;

import com.avaloq.model.DiceConfiguration;
import com.avaloq.model.DiceSimulation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.PreparedStatementCreator;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import org.springframework.stereotype.Repository;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

@Slf4j
@Repository
public class DiceDaoImpl implements DiceDao {

    public static final String INSERT_DICE_SQL = "INSERT INTO dice(no_dice, total_side, total_no_rolls)VALUES (?,?,?)";
    public static final String GET_SIMULATION_SQL = "SELECT no_dice, total_side, SUM (total_no_rolls) as total_rolls , Count(*) as total_count from DICE group by no_dice, total_side";

    private final JdbcTemplate jdbcTemplate;

    public DiceDaoImpl(JdbcTemplate jdbcTemplate) {
        this.jdbcTemplate = jdbcTemplate;
    }


    /**
     * Save DiceConfiguration configuration
     * @param diceConfiguration
     * @return
     */
    @Override
    public long saveDice(final DiceConfiguration diceConfiguration) {
        KeyHolder holder = new GeneratedKeyHolder();
        jdbcTemplate.update(new PreparedStatementCreator() {
            @Override
            public PreparedStatement createPreparedStatement(Connection connection) throws SQLException {
                PreparedStatement ps = connection.prepareStatement(INSERT_DICE_SQL, Statement.RETURN_GENERATED_KEYS);
                ps.setInt(1, diceConfiguration.getDiceNumber());
                ps.setInt(2, diceConfiguration.getTotalSide());
                ps.setInt(3, diceConfiguration.getTotalNoRolls());
                return ps;
            }
        }, holder);
        long newDiceId = holder.getKey().longValue();
        log.info("The new id for DiceConfiguration obj: {} is: {}", diceConfiguration, newDiceId);
        return newDiceId;
    }


    /**
     * Get DiceConfiguration Simulation  - Some of the business logic is being done at the DB level for efficency, please check the SQL native query
     * @return
     */
    @Override
    public List<DiceSimulation> getDiceSimulation() {
        List<Map<String, Object>> maps = jdbcTemplate.queryForList(GET_SIMULATION_SQL);
        return getDiceSimulationTotal(maps);
    }

    private List<DiceSimulation> getDiceSimulationTotal(final List<Map<String, Object>> maps) {
        List<DiceSimulation> diceSimulations = new ArrayList<>();
        maps.stream().forEach(element -> diceSimulations.add(new DiceSimulation(
                Integer.parseInt(String.valueOf(element.get("no_dice"))),
                Integer.parseInt(String.valueOf(element.get("total_side"))),
                Integer.parseInt(String.valueOf(element.get("total_rolls"))),
                Integer.parseInt(String.valueOf(element.get("total_count")))
        )));
        log.info("Relative Simulation list size: {}", diceSimulations.size());
        return diceSimulations;
    }


}
