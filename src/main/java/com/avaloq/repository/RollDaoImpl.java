package com.avaloq.repository;


import com.avaloq.model.DiceRelativeDistribution;
import com.avaloq.model.Roll;
import lombok.extern.slf4j.Slf4j;
import org.springframework.jdbc.core.BatchPreparedStatementSetter;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

@Slf4j
@Repository
public class RollDaoImpl implements RollDao {

    public static final String INSERT_ROLL_SQL = "INSERT INTO roll (sum, dice_id)VALUES (?,?)";
    public static final String RELATIVE_DISTRIBUTION_SQL = "SELECT sum, SUM (sum) as sum_total , Count(*) as roll_count FROM roll WHERE dice_id IN (select id from dice where no_dice=? and total_side=?) GROUP BY sum";

    private final JdbcTemplate jdbcTemplate;

    public RollDaoImpl(JdbcTemplate jdbcTemplate) {
        this.jdbcTemplate = jdbcTemplate;
    }


    /**
     * Batch Insert of every dice roll
     * @param executedRolls
     * @return
     */
    public int[] batchRollsInsert(List<Roll> executedRolls) {
        int[] executed = this.jdbcTemplate.batchUpdate(INSERT_ROLL_SQL,
                new BatchPreparedStatementSetter() {

                    public void setValues(PreparedStatement ps, int i)
                            throws SQLException {
                        ps.setLong(1, executedRolls.get(i).getSum());
                        ps.setLong(2, executedRolls.get(i).getDiceId());
                    }
                    public int getBatchSize() {
                        return executedRolls.size();
                    }
                });
        log.info("Roll execution completed with a total of: {} executed", executed);
        return executed;
    }

    /**
     * Get the Relative Distribution  - Some of the business logic is being done at the DB level for efficency, please check the SQL native query
     * @param noDice
     * @param sideTotal
     * @return
     */
    @Override
    public List<DiceRelativeDistribution> getRelativeDistribution(int noDice, int sideTotal) {
        List<Map<String, Object>> maps = jdbcTemplate.queryForList(RELATIVE_DISTRIBUTION_SQL, noDice, sideTotal);
        return getRelativeDistributionResult(maps);
    }

    private List<DiceRelativeDistribution> getRelativeDistributionResult(final List<Map<String, Object>> maps) {
        List<DiceRelativeDistribution> diceRelativeDistributions = new ArrayList<>();
        maps.stream().forEach(element -> diceRelativeDistributions.add(new DiceRelativeDistribution(
                Integer.parseInt(String.valueOf(element.get("sum"))),
                Integer.parseInt(String.valueOf(element.get("sum_total"))),
                Integer.parseInt(String.valueOf(element.get("roll_count")))
        )));
        log.info("Relative Distribution list size: {}", diceRelativeDistributions.size());
        return diceRelativeDistributions;
    }


}
