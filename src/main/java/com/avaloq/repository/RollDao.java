package com.avaloq.repository;

import com.avaloq.model.DiceRelativeDistribution;
import com.avaloq.model.Roll;

import java.util.List;

public interface RollDao {
    int[] batchRollsInsert(List<Roll> executedRolls);

    List<DiceRelativeDistribution> getRelativeDistribution(int noDice, int sideTotal);
}
