package com.avaloq.controller;

import com.avaloq.exceptions.ErrorMessage;
import com.avaloq.model.DiceRelativeDistribution;
import com.avaloq.model.DiceSimulation;
import com.avaloq.service.DiceDistributionService;
import com.avaloq.service.DiceSimulationReportService;
import io.swagger.annotations.ApiOperation;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("v1/api")
public class DiceController {

    private final DiceSimulationReportService diceSimulationReportService;
    private final DiceDistributionService diceDistributionService;

    public DiceController(DiceSimulationReportService diceSimulationReportService, DiceDistributionService diceDistributionService) {
        this.diceSimulationReportService = diceSimulationReportService;
        this.diceDistributionService = diceDistributionService;
    }


    @GetMapping(value = "/simulation-report")
    @ApiOperation(value = "Return the total number of simulations and total rolls made", notes = "Return the total number of simulations and total rolls made, grouped by all existing dice number–dice side\n" +
            "combinations.")
    public ResponseEntity<List<DiceSimulation>> getAllSimulationsAndRolls() {
        return new ResponseEntity<>(diceSimulationReportService.getDiceSimulation(), HttpStatus.OK);
    }


    @GetMapping(value = "/relative-distribution")
    @ApiOperation(value = "For a given dice number–dice side combination", notes = "Return the relative distribution, compared to the total rolls, for all\n" +
            "the simulations.")
    public ResponseEntity<List<DiceRelativeDistribution>> getAllSimulationsAndRolls(@RequestParam(required = true) int diceNumber,
                                                                                    @RequestParam(required = true) int diceSides) {
        return new ResponseEntity<>(diceSimulationReportService.getRelativeDistribution(diceNumber, diceSides), HttpStatus.OK);
    }


    @ApiOperation(value = "Execute a dice roll", notes = "The number of dice, sides of the dice and the total number of rolls is configurable through query\n" +
            "parameters.")
    @PostMapping(value = "/execute-roll")
    public ResponseEntity<?> executeDiceRoll(@RequestParam(required = true) int diceNumber,
                                             @RequestParam(required = true) int totalNoRoll,
                                             @RequestParam(required = true) int diceSides) {
        /**
         * FIXME - A more elegant solution here is to use an object as payload which properties can be validated using
         * @see {@link org.hibernate.validator.constraints}
         * Since the Requirement specified to use Query Parameters, I had to use query params and write a custom validator.
         */
        List<ErrorMessage> errorMessages = diceDistributionService.validateInput(diceNumber, totalNoRoll, diceSides);
        if (errorMessages.isEmpty()) {
            Map<Integer, Long> longMap = diceDistributionService.executeDiceDistribution(diceNumber, totalNoRoll, diceSides);
            return new ResponseEntity<>(longMap, HttpStatus.CREATED);
        }
        return new ResponseEntity<>(errorMessages, HttpStatus.BAD_REQUEST);
    }

}
