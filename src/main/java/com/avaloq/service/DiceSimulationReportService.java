package com.avaloq.service;

import com.avaloq.model.DiceRelativeDistribution;
import com.avaloq.model.DiceSimulation;
import com.avaloq.repository.DiceDao;
import com.avaloq.repository.RollDao;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.List;
import java.util.stream.Collectors;

@Slf4j
@Service
public class DiceSimulationReportService {

    private final DiceDao diceDao;
    private final RollDao rollDao;

    public DiceSimulationReportService(DiceDao diceDao, RollDao rollDao) {
        this.diceDao = diceDao;
        this.rollDao = rollDao;
    }

    /**
     * Return the total number of simulations and total rolls made, grouped by all existing dice number–dice side
     * combinations.
     * a. Eg. if there were two calls to the REST endpoint for 3 pieces of 6 sided dice, once with a total number of
     * rolls of 100 and once with a total number of rolls of 200, then there were a total of 2 simulations, with a
     * total of 300 rolls for this combination.
     *
     * @return
     */
    public List<DiceSimulation> getDiceSimulation() {
        //Check the diceDAO as the SQL query does the group by calculations
        return diceDao.getDiceSimulation();
    }

    /**
     * For a given dice number–dice side combination, return the relative distribution, compared to the total rolls, for all
     * the simulations.
     * a. In case of a total of 300 rolls, if the sum „3” was rolled 4 times, that would be 1.33%.
     * b. If the sum „4” was rolled 3 times, that would be 1%.
     * c. If the total „5” was rolled 11 times, that would be 3.66%. Etc...
     *
     * @param noDice
     * @param sideTotal
     * @return
     */
    public List<DiceRelativeDistribution> getRelativeDistribution(int noDice, int sideTotal) {
        List<DiceRelativeDistribution> relativeDistributionList = rollDao.getRelativeDistribution(noDice, sideTotal);
        log.info("Relative Distribution returned a size of: {} for the params noDice:{} and sideTotal:{}", relativeDistributionList.size(), noDice, sideTotal);
        return relativeDistributionList.stream()
                .map(dis -> new DiceRelativeDistribution(dis.getSum(), dis.getSumTotal(), dis.getRollCount(),
                        BigDecimal.valueOf((double) dis.getRollCount() / (double) dis.getSum()).setScale(2, RoundingMode.HALF_UP).doubleValue()))
                .collect(Collectors.toList());
    }
}
