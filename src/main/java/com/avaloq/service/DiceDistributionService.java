package com.avaloq.service;


import com.avaloq.exceptions.ErrorMessage;
import com.avaloq.model.DiceConfiguration;
import com.avaloq.model.Roll;
import com.avaloq.repository.DiceDao;
import com.avaloq.repository.RollDao;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.function.Function;
import java.util.stream.Collectors;
import java.util.stream.IntStream;
import java.util.stream.Stream;


@Slf4j
@Service
public class DiceDistributionService {

    private final DiceDao diceDao;
    private final RollDao rollDao;

    public DiceDistributionService(DiceDao diceDao, RollDao rollDao) {
        this.diceDao = diceDao;
        this.rollDao = rollDao;
    }

    /**
     * Calculate Frequency
     * @param elements
     * @param <Integer>
     * @return
     */
    public static <Integer> Map<Integer, Long> frequencyMap(Stream<Integer> elements) {
        return elements.collect(
                Collectors.groupingBy(
                        Function.identity(),
                        //HashMap::new, // can be skipped
                        Collectors.counting()
                )
        );
    }

    /**
     * Roll N pieces of N-sided dice a total of N times.
     * a. For every roll sum the rolled number from the dice.
     * b. Count how many times each total has been rolled.
     * c. Return this as a JSON structure.
     *
     * @param diceNumber
     * @param totalNoRoll
     * @param diceSides
     * @return
     */
    @Transactional
    public Map<Integer, Long> executeDiceDistribution(int diceNumber, int totalNoRoll, int diceSides) {
        List<Roll> rolls = new ArrayList<>();
        int[] res = IntStream.rangeClosed(1, totalNoRoll).map(x -> {
            int sum = 0;
            for (int i = 1; i <= diceNumber; i++) {
                int dice = (int) (Math.random() * diceSides + 1);
                sum += dice;
            }
            rolls.add(new Roll(sum));
            return sum;
        }).toArray();

        long diceId = diceDao.saveDice(new DiceConfiguration(diceNumber, diceSides, totalNoRoll));
        saveAllRolls(diceId, rolls);

        //Count how many times each total has been rolled.
        return frequencyMap(Arrays.stream(res).boxed());
    }

    /**
     * Save all dice rolls
     * @param diceId
     * @param rolls
     * @return
     */
    @Transactional
    public int[] saveAllRolls(long diceId, List<Roll> rolls) {
        log.info("Saving {} number of dice rolls with diceId: {}", rolls.size(), diceId);
        List<Roll> rollsToSave = rolls
                .stream()
                .map(item -> new Roll(item.getSum(), diceId))
                .collect(Collectors.toList());
        return rollDao.batchRollsInsert(rollsToSave);
    }


    /**
     * Add input validation:
     * a. The number of dice and the total number of rolls must be at least 1.
     * b. The sides of a dice must be at least 4.
     *
     * @param diceNumber
     * @param totalNoRoll
     * @param diceSides
     * @return
     */
    public List<ErrorMessage> validateInput(int diceNumber, int totalNoRoll, int diceSides) {
        List<ErrorMessage> errorMessages = new ArrayList<>();

        if (diceNumber < 1) {
            ErrorMessage ex = new ErrorMessage(HttpStatus.BAD_REQUEST.value(), "Provided:" + diceNumber, "DiceConfiguration Number must be at least 1");
            errorMessages.add(ex);
        } else if (totalNoRoll < 1) {
            ErrorMessage ex = new ErrorMessage(HttpStatus.BAD_REQUEST.value(), "Provided: " + totalNoRoll, "Total Number of Rolls must be at least 1");
            errorMessages.add(ex);
        } else if (diceSides < 4) {
            ErrorMessage ex = new ErrorMessage(HttpStatus.BAD_REQUEST.value(), "Provided: " + diceSides, "DiceConfiguration sides must be at least 4");
            errorMessages.add(ex);
        }
        return errorMessages;
    }


}
