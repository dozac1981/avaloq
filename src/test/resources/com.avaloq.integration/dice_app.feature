Feature: OPERATIONS on Roll Dice

  Scenario: Execute a Roll Dice
    When I execute a roll
    Then I get a 'CREATED' response
    Then Check content that validates the expected output


  Scenario: After executing a role, get Simulation Report
    Given I execute a roll
    When I get a simulation report
    Then I get a 'OK' response

  Scenario: After executing a role, get Simulation Report
    Given I execute a roll
    When I get a relative distribution for a given dice number–dice side combination
    Then Check relative distribution content that validates the expected output









