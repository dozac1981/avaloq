package com.avaloq.integration;

public class IntegrationTestConstants {

    public static final String BASE_URI = "http://localhost:5555";

    public static final String API_URI = BASE_URI + "/v1/api";
}
