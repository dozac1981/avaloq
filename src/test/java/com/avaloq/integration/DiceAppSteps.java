package com.avaloq.integration;


import com.avaloq.model.DiceRelativeDistribution;
import com.avaloq.model.DiceSimulation;
import com.fasterxml.jackson.databind.JsonNode;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponentsBuilder;

import java.net.URI;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;


@CucumberStepsDefinition
public class DiceAppSteps {

    @Autowired
    private RestTemplate restTemplate;

    @Autowired
    private Glue glue;

    @When("^I execute a roll$")
    public void I_execute_a_roll() throws Throwable {
        final ResponseEntity<JsonNode> responseEntity = tryToExecuteADiceRool();
        saveResponse(responseEntity);
    }

    @Then("^Check content that validates the expected output$")
    public void the_client_receives_status_code_of() throws Throwable {
        glue.getResponse().getStatusCode();
        JsonNode map = (JsonNode) glue.getResponse().getBody();
        assertThat(map.size() == 5);
    }


    @Given("^I get a simulation report$")
    public void get_simulation_report() throws Throwable {
        final ResponseEntity<DiceSimulation[]> responseEntity = tryToGetSimulationReport();
        saveResponse(responseEntity);
    }

    @Then("^Check simulation report is not empty, hence contains an output$")
    public void check_simulation_report() throws Throwable {
        glue.getResponse().getStatusCode();
        DiceSimulation[] simulations = (DiceSimulation[]) glue.getResponse().getBody();
        List<DiceSimulation> diceSimulations = new ArrayList<>(Arrays.asList(simulations));
        assertThat(!diceSimulations.isEmpty());
    }

    @Given("^I get a relative distribution for a given dice number–dice side combination$")
    public void get_a_relative_distribution() throws Throwable {
        final ResponseEntity<DiceRelativeDistribution[]> responseEntity = tryToGetRelativeDistributionReport();
        saveResponse(responseEntity);
    }

    @Then("^Check relative distribution content that validates the expected output$")
    public void return_the_relative_distribution() throws Throwable {
        glue.getResponse().getStatusCode();
        DiceRelativeDistribution[] diceRelativeDistributions = (DiceRelativeDistribution[]) glue.getResponse().getBody();
        List<DiceRelativeDistribution> distributions = new ArrayList<>(Arrays.asList(diceRelativeDistributions));
        assertThat(!distributions.isEmpty());
    }

    //TODO there are more uses cases to cover
    public void saveResponse(final ResponseEntity responseEntity) {
        glue.setResponse(responseEntity);
    }


    private ResponseEntity<JsonNode> tryToExecuteADiceRool() {
        URI uri = UriComponentsBuilder.fromUriString(IntegrationTestConstants.API_URI.concat("/execute-roll"))
                .buildAndExpand()
                .toUri();
        uri = UriComponentsBuilder
                .fromUri(uri)
                .queryParam("diceNumber", 4)
                .queryParam("totalNoRoll", 6)
                .queryParam("diceSides", 4)
                .build()
                .toUri();

        return restTemplate.postForEntity(uri, HttpMethod.POST, JsonNode.class);

    }


    private ResponseEntity<DiceSimulation[]> tryToGetSimulationReport() {
        return restTemplate.exchange(
                IntegrationTestConstants.API_URI.concat("/simulation-report"),
                HttpMethod.GET,
                null,
                DiceSimulation[].class);
    }

    private ResponseEntity<DiceRelativeDistribution[]> tryToGetRelativeDistributionReport() {
        URI uri = UriComponentsBuilder.fromUriString(IntegrationTestConstants.API_URI.concat("/relative-distribution"))
                .buildAndExpand()
                .toUri();
        uri = UriComponentsBuilder
                .fromUri(uri)
                .queryParam("diceNumber", 4)
                .queryParam("diceSides", 4)
                .build()
                .toUri();
        return restTemplate.exchange(uri, HttpMethod.GET, null, DiceRelativeDistribution[].class);
    }
}
