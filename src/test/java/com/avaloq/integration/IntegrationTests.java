package com.avaloq.integration;

import cucumber.api.CucumberOptions;
import cucumber.api.junit.Cucumber;
import org.junit.runner.RunWith;

@RunWith(Cucumber.class)
@CucumberOptions(features = "classpath:com.avaloq.integration", plugin = {"pretty",
        "json:target/cucumber-report.json"})
public class IntegrationTests {

}
