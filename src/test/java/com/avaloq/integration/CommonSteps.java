package com.avaloq.integration;

import cucumber.api.java.en.Then;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertThat;


public class CommonSteps {

    @Autowired
    private Glue glue;

    @Then("^(?:I get|the user gets) a '(.*)' response$")
    public void I_get_a__response(final String statusCode) throws Throwable {
        final ResponseEntity response = glue.getResponse();
        assertThat(response.getStatusCode(), is(HttpStatus.valueOf(statusCode)));
    }
}
