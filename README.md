# Notes for the code Reviewer 

## Overview
1. The test would run with java 1.8.
2. Includes Swagger API docs.
3. Includes Cucumber test Spring integration.
5. Spring JBDC Template used for persistence. (prefered over the classic Spring Data JPA abstraction for performance reasons). 


## Guidelines
The test should run out of the box if imported in intellij
At startup the Resources folder contains a schema.sql for the tables required to run the app.

## access H2 Console
You can access H2 db console using the link below in case of wanting to query the tables.
http://localhost:8080/h2-console/login.do?
                                                       
## Swagger Calls
For testing the functionality as provided in the Test specification, please use Swagger via http://localhost:8080/swagger-ui.html

## Running Cucumber Integration tests for all futures as per the Requirements
[Main class] (https://bitbucket.org/dozac1981/avaloq/src/master/src/test/java/com/avaloq/integration/IntegrationTests.java)


